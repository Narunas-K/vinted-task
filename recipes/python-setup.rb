#
# Cookbook:: vinted
# Recipe:: python-setup
#
# Copyright:: 2019, The Authors, All Rights Reserved.


#python and it's libraries setup
package 'python' do
  action :install
end

package 'python-pip' do
  action :install
end

execute 'install-redis-lib' do
  command 'pip install redis'
end

execute 'install-mailbox-lib' do
  command 'pip install mailbox'
end

