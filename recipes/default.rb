#
# Cookbook:: vinted
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

include_recipe 'vinted::email-redis'
include_recipe 'vinted::python-setup'
include_recipe 'vinted::deploy-script'