#
# Cookbook:: vinted
# Recipe:: deploy-script
#
# Copyright:: 2019, The Authors, All Rights Reserved.


directory "/etc/scripts/" do
  action :create
  owner 'root'
  group 'root'
  mode '0770'
end

cookbook_file "/etc/scripts/email-check-script.py" do
  source 'email-check-script.py'
  owner 'root'
  group 'root'
  mode '770'
end

cron 'email-check' do
  action :create
  minute '*'
  hour '*'
  weekday '*'
  month '*'
  user 'root'
  command 'python /etc/scripts/email-check-script.py'
end

execute 'send-test-mail' do
	command "printf 'This is a test message' | exim -v vagrant@localhost.localdomain ; sleep 5 ; printf '\n'"
end 