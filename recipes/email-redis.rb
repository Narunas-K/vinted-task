#
# Cookbook:: vinted
# Recipe:: email-redis
#
# Copyright:: 2019, The Authors, All Rights Reserved.


package 'epel-release' do
  action :install
end

#Exim mail server and redis server setup
package 'exim' do
  action :install
end

package 'redis' do
  action :install
end

service 'exim' do
  action [ :enable, :start ]
end

service 'redis' do
  action [ :enable, :start ]
end






