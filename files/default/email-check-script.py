__author__ = "Narunas Kapocius, 2018-03-03"

import redis # to communicate with redis server
from os import listdir
from os.path import isfile, join
import mailbox # to easily read emails from email_dir
import email

# redis connection configs
redis_host = "localhost"
redis_port = 6379

# configure Redis connection
r = redis.StrictRedis(host = redis_host, port = redis_port)

# variables
domain = "@localhost.localdomain"
email_dir = "/var/spool/mail/"
file_list = []
write_completed = False

# list all files in the /var/spool/mail/ directory and write them to the list
try:
  for item in listdir(email_dir):
    if isfile(join(email_dir, item)):
      file_list.append(item)
except Exception as e:
  print("Error occured: " + str(e))
# print file_list

# read each file in /var/spoll/mail/ directory and write content of that file to redis database
if file_list !=[]:
  for item in file_list:
    write_completed = False
    email_file = open(join(email_dir, item), 'r')
    mbox = mailbox.UnixMailbox(email_file, email.message_from_file)


    for message in mbox:
      try:
        r.rpush(item+domain, str(message))
        write_completed = True
      except Exception as e:
        print("Error occured: " + str(e))
        write_completed = False
        break

        # clean each file in /var/spool/mail/ directory
    if write_completed == True:
      email_file = open(join(email_dir, item), 'w')
      email_file.write('')
      email_file.closed

    email_file.closed
