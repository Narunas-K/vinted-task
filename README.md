# Vinted Chef Cookbook

The Chef cookbook installs exim mail server and redis server. All email server's received messages are stored in redis server and could be queried using receivers' "rcpt to" address. Mail server and redis server is connected using Python2 script. The cookbook is compatible with CentOS 7 OS.

# Running the tests

The cookbook can be tested with Test Kitchen in two ways:
### I. Automated test:
1. Inside repository from command line run the command:
``
kitchen test
``
2. The test result will be displayed in the command line

### II. Test with ability to check the VM status manually using SSH:
1. Run:
``
kitchen converge
``
2. Run:
``
kitchen verify
``
3. SSH to VM instance: find the IP address and port number in the command line output. Username and password:
``
vagrant
``
4. Send email to vagrant user through exim: 
``
printf 'Hello, This is a test message. Bye.' | exim -v vagrant@localhost.localdomain; printf '\n'
``
5. After a minute query the Redis server, to get all messages for vagrant user: 
``
redis-cli lrange vagrant@localhost.localdomain 0 -1
``
6. Destory VM:
``
kitchen destroy
``
