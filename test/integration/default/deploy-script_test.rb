# # encoding: utf-8

# Inspec test for recipe vinted::deploy-script

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/


control 'check-python-script' do
	describe file('/etc/scripts/email-check-script.py') do
		its('mode') { should cmp '0770' }
		its('owner') { should eq 'root' }
		its('group') { should eq 'root' }
	end

		#check crontab
	describe crontab('root') do
		its('commands') { should include 'python /etc/scripts/email-check-script.py' }
	end
end

control 'test-exim-redis-connection' do

	describe command("python /etc/scripts/email-check-script.py") do
    	its('exit_status') { should eq 0}
	end

	describe command('redis-cli lrange vagrant@localhost.localdomain 0 -1') do
    	 its('stdout') { should match (/This is a test message/) }
	end
end