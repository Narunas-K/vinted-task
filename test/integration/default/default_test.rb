# # encoding: utf-8

# Inspec test for recipe vinted::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

# control 'redis-check' do
# 	# checking redis package, service status
# 	describe service('redis') do
# 		it { should be_enabled }
# 		it { should be_installed }
# 		it { should be_running }
# 	end

# 	# checking redis port
# 	describe port(6379) do
# 		it {should be_listening}
# 		its('processes') {should include 'redis-server'}
# 	end

# end

# control 'exim-check' do

# 	describe service('exim') do
# 		it { should be_enabled }
# 		it { should be_installed }
# 		it { should be_running }
# 	end

# 	# checking exim ports 465, 587 and 25
# 	describe port(465) do
# 		it {should be_listening}
# 		its('processes') {should include 'exim'}
# 	end

# 	describe port(587) do
# 		it {should be_listening}
# 		its('processes') {should include 'exim'}
# 	end

# 	describe port(25) do
# 		it {should be_listening}
# 		its('processes') {should include 'exim'}
# 	end
# end


# control 'python-check' do
# 	# check python, and python related packages
# 	describe package('python') do
# 		it { should be_installed }
# 	end

# 	describe pip('redis') do
# 	  it { should be_installed }
# 	end

# 	describe pip('mailbox') do
# 	  it { should be_installed }
# 	end
# end


# control 'crontab-check' do
# 	#check crontab
# 	describe crontab('root') do
# 		its('commands') { should include 'python /etc/scripts/email-check-script.py' }
# 	end
# end


# control 'check-python-script' do
# 	describe file('/etc/scripts/email-check-script.py') do
# 		its('mode') { should cmp '0770' }
# 		its('owner') { should eq 'root' }
# 		its('group') { should eq 'root' }

# 	end
# end