# # encoding: utf-8

# Inspec test for recipe vinted::email-redis

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

control 'redis-check' do
	# checking redis package, service status
	describe service('redis') do
		it { should be_enabled }
		it { should be_installed }
		it { should be_running }
	end

	# checking redis port
	describe port(6379) do
		it {should be_listening}
		its('processes') {should include 'redis-server'}
	end

end

control 'exim-check' do

	describe service('exim') do
		it { should be_enabled }
		it { should be_installed }
		it { should be_running }
	end

	# checking exim ports 465, 587 and 25
	describe port(465) do
		it {should be_listening}
		its('processes') {should include 'exim'}
	end

	describe port(587) do
		it {should be_listening}
		its('processes') {should include 'exim'}
	end

	describe port(25) do
		it {should be_listening}
		its('processes') {should include 'exim'}
	end
end