# # encoding: utf-8

# Inspec test for recipe vinted::python-setup

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

control 'python-check' do
	# check python, and python related packages
	describe package('python') do
		it { should be_installed }
	end

	describe pip('redis') do
	  it { should be_installed }
	end

	describe pip('mailbox') do
	  it { should be_installed }
	end
end